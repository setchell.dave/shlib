if test -z "$tmp"
then
	tmp=$(mktemp -d)
fi
test -d "$tmp" || { echo failed to tempify; exit 3 ;}
mkdir -p "$tmp/.ctx"

trap '_cleanup' EXIT INT TERM HUP # there is a lot of conflicting info on INT TERM...

_pre_clean () { :; }  # redefine for your app

_cleanup () { # called on exit
	_pre_clean
	while test -s "${tmp}/.ctx/stack" ## TODO dangerous run forever
	do
		echo popping $(tail -1 $tmp/.ctx/stack)
		_with_out
	done >/dev/null
	_post_clean
	test -d "$tmp" && rm -rf "$tmp"
}

_post_clean () { :; } # redefine for your app

_fullpath () { # directory -> full pathy
	test -d "$1" && (cd "$1"; pwd -P)
	test -f "$1" && (cd "$(dirname $1)"; printf '%s/%s\n' "$(pwd -P)" "$(basename $1)")
}

_pathmunge () {
	_V_="$1"
	_XX_="$(eval echo \$$1)"; test -n "$_XX_" || return 1; shift
	while test -n "$1"
	do case :$_XX_: in *:$1:*):;; *) _XX_="$1:$_XX_" ;; esac; shift ; done
	eval $_V_='$_XX_'; unset _V_ _XX_
}

_make_id () {
	case "$1" in
	user)
		a=$(find . -maxdepth 1 -name "$n4" | xargs -r -n1 basename | sort -nr | head -1)
		if test -z "$a"
		then a=1000
		else a=$(printf '%0.4d' $(( $a + 1 )))
		fi
		echo $a
		;;
	add)
		a=$(find . -maxdepth 1 -name "$n3" | xargs -r -n1 basename | sort -nr | head -1)
		if test -z "$a"
		then a=000
		else a=$(printf '%0.3d' $(( $a + 1 )))
		fi
		echo $a
		;;
	*) _r 12 hex ;;
	esac
}

_norm () {
	case "$1" in
	*M|*m) echo $(( ${1%?} * 1024 * 1000 )) ;;
	*K|*k) echo $(( ${1%?} * 1024 )) ;;
	*G|*g) echo $(( ${1%?} * 1024 * 1000 * 1000 )) ;;
	*) echo $1 ;;
	esac
}

_new_id () { # directory -> generator -> id
	mkdir -p $1 || return 1
	(cd $1; shift
	id=$("$@")
	while test -e "./$id"
	do id=$("$@"); done
	mkdir -p "./$id"
	echo $id)
}

_search_id () {
	(cd $1; fname="$2"; shift; shift
	IFS=,; set -- "$1"; unset IFS
	for match in "$@"
	do
		case "$match" in
		'.'|'..') :;;
		*[!0-9]*) test -d ./"$match" && { echo "$match"; continue ;} ;;
		*)
			test -d ./$(printf '%0.3d' "$match") && { printf '%0.3d\n' "$match"; continue ;}
			continue
			;;
		esac
		for x in $(find . -type f -name "$fname" | xargs -r file | grep ASCII \
					| cut -d: -f1 | xargs -r grep -l "$match")
		do echo $x | cut -d/ -f2; done
	done)
}

_form () { # stdin:format -> args... -> formatted output
	test -t 0 && return 1
	printf "$(cat -)\n" "$@"
}

_error () {  # args -> echos args to error channel and exits
	echo "$@" >&2
	exit 1
}

_log () { # level -> msg -> log written
	__LLVL=$(echo "$1" | tr a-z A-Z); shift
	printf '%s|%s|%s\n' $(date -Is) "${__LLVL}" "$*" >>${OGDIR}/.log/$(date -I).$(basename $0).log
	unset __LLVL __LTEE
}

_with () {  # ctx -> name -> args -> Monad context 
	test -z "$2" && _error must provide context and name
	_in_"$@" && { mkdir -p $tmp/.ctx/$1/$2 
				  printf '%s|%s\n' "$1" "$2" >>"$tmp/.ctx/stack" ;}
}

_with_out () {  # args... -> pop ctx to args...
	# TODO seems a bit heafty to handle a pop chain
	test -s ${tmp}/.ctx/stack || return 1 
	if test -n "$1"
	then
		while test -n "$1"
		do
			test $(tail -1 $tmp/.ctx/stack | cut -d\| -f1) = "$1" &&  shift
			eval _out_$(tail -1 $tmp/.ctx/stack | tr \| ' ')
			sed -i '$d' "${tmp}/.ctx/stack"
		done
	else
		eval _out_$(tail -1 $tmp/.ctx/stack | tr \| ' ')
		sed -i '$d' "${tmp}/.ctx/stack"
	fi
}

_in_stash () { # name -> in stash context
	git diff --cached --exit-code \
		&& git diff --exit-code 2>&1 1>/dev/null && return 1
	git stash push -a -m "[$1]"
}

_out_stash () { # ctx name [ overloaded w/ stash name ] -> popped out
    _sN=$(git stash list | grep "\[$1\]")
    test -z "$_sN" && return 0
    _sN="${_sN#*\{}"; _sN="${_sN%%\}*}"
    git stash pop --index ${_sN} >/dev/null 2>&1
    unset _sN
}

_in_clone () { # name -> remote -> [branch] -> ctx
	mkdir -p $tmp/.ctx/clone
	(cd "$tmp/.ctx/clone"
	git clone "$2" "$1" || _error failed to clone
	test -n "$3" && (cd "$1"; git checkout "$3")
	)
}

_cmd_clone () {
	__clone_dir="$tmp/.ctx/clone/$1"
	(cd $__clone_dir
	case "$2" in
	shell) _shell C $1 ;;
	*) shift; "$@" ;;
	esac)
}

_out_clone () { 
	rm -rf "$tmp/.ctx/clone/$1"
}

_branch_exists () { # [branch name] -> boolean
	{ git rev-parse --verify ${1:-master} \
	    || git rev-parse --verify remotes/origin/${1:-master} ;} >/dev/null 2>&1
}

_in_branch () { # name -> checked out ctx
	__bdir="$tmp/.ctx/branch/$1"; mkdir -p "$__bdir"
    git rev-parse --abbrev-ref HEAD >"$__bdir/previous"
	test "$1" = $(cat $__bdir/previous) && return 0
	_with stash $1
	if _branch_exists $1
	then
	    git checkout $1
	    git pull
	else
	    git checkout -b $1
	    $__SHLIB_GIT_PUSH && git push -u origin HEAD
	fi 
	return 0
}

_out_branch () { # ctx name -> out of branch context
    git diff --exit-code || git add -u
    git diff --exit-code || _error git add failed
    git diff --cached --exit-code \
		|| git commit -m"automatic add to branch $1)"
    git diff --cached --exit-code || _error git commit failed 
	$__SHLIB_GIT_PUSH && { git push $__SHLIB_PUSH_FORCE \
		|| _error failed to push despite being asked. ;}
    git checkout $(cat $tmp/.ctx/branch/$1/previous)
}

_in_loopback () { # name -> file -> mounted ma' dawg
	test -z "$2" && return 1
	__ldir="$tmp/.ctx/loopback/$1"; mkdir -p "$__ldir"
	echo $2 >$__ldir/file
	sudo losetup -f -P --show "$2" >"$__ldir/device" || return 1
	mkdir -p $__ldir/mountpoint
	for x in $(cat "$__ldir/device")p*
	do
		btype=$(sudo blkid -s TYPE "$x" | tr -d '"' | cut -d= -f2)
		test "$btype" = "swap" && continue
		mkdir -p "$__ldir/mountpoint/${x##*p}"
		sudo mount $x "$__ldir/mountpoint/${x##*p}" 2>/dev/null
	done
}

_cmd_loop () {
	__pdir=$tmp/.ctx/loopback/$1
	case $2 in
	tree) tree $__pdir/mountpoint | sed 1d ;;
	mountpoint) echo $__pdir/mountpoint ;;
	esac
}

_out_loopback () { # ctx name -> Monad unmounted
	for x in $(ls "$tmp/.ctx/loopback/$1/mountpoint/")
	do sudo umount "$tmp/.ctx/loopback/$1/mountpoint/$x"; done
	sudo losetup -d "$(cat $tmp/.ctx/loopback/$1/device)"
}

_chan_read () { # name -> fd -> command -> [args...] -> ()
	__cdir=$tmp/.ctx/chan/$1; shift
	__CHAN_CMD="$1"; shift
	sed 's/^/./' | while read __CHANREAD
	do
		__CHANREAD="${__CHANREAD#.}";
		case "$__CHANREAD" in
		__EXIT_CHAN__) _clear; return 0 ;;
		*) $__CHAN_CMD "$@" "$__CHANREAD" ;;
		esac
	done 
	unset __CHANREAD
}

_cmd_chan () {
	__cdir=$tmp/.ctx/chan/$1
	case $2 in
	exit|close) echo __EXIT_CHAN__ >$__cdir/fifo ;;
	wait) wait $(cat $__cdir/pid) ;;
	exists) test -f $__cdir/fifo ;;
	tee) tee $__cdir/fifo ;;
	in) cat - >$__cdir/fifo ;;
    list) ls -t $__cdir/.. ;;
    pipe) echo $__cdir/fifo ;;
	esac
}

_in_chan () { # name -> processor -> [args...] -> Channel
	__cdir="$tmp/.ctx/chan/$1"; mkdir -p $__cdir
	test -e "${__cdir}/fifo" && return 1
	mkfifo $__cdir/fifo
	<$__cdir/fifo _chan_read "$@" &
	echo $! >$__cdir/pid
	jobs -l | grep $(cat $__cdir/pid) | cut -c2- | cut -d\] -f1 >$__cdir/jobid
}

_out_chan () { # ctx name -> channel closed
	__cdir=$tmp/.ctx/chan/$1
	ps -p $(cat $__cdir/pid) >/dev/null && { kill "%"$(cat $__cdir/jobid)
		 wait $(cat $__cdir/pid) ;}
}

_in_state () { # name -> state
	test -z "$1" && return 1
	__sdir=$tmp/.ctx/state/$1; mkdir -p "$__sdir"
}

_cmd_state () {
	(__sdir=$tmp/.ctx/state/$1; mkdir -p $__sdir
	 __sfile=$__sdir/${3:-default}
	touch $__sfile; tmpfile=$(mktemp -p $tmp); cp $__sfile $tmpfile
	case "$2" in
	extant) test -s $__sfile || return 1 ;;
	lines) wc -l <$__sfile ;;
	read) cat $__sfile ;;
	update) cat >$tmpfile; cp $tmpfile $__sfile ;;
	append) cat >>$tmpfile; cp $tmpfile $__sfile ;;
	delete) rm $__sfile ;;
    struct) tree $__sdir ;;
	raw) echo $__sfile ;;
	esac
	)
}

_out_state () { # 
	rm -r "$tmp/.ctx/state/$1"
}


_wait () { # command -> doi
	"$@" 2>/dev/null & __WAIT_PID=$!; __WAIT_START=$(date +%s)
	while ps -p $__WAIT_PID >/dev/null
	do
		__msg=$(printf '[%3.3s] pid: %d proc: %s' $(_spin $(( $(date +%s) - $__WAIT_START )) 3) $__WAIT_PID $1)
		_scan "$__msg" >/dev/tty
		sleep 0.2
	done
	unset __WAIT_PID __WAIT_START
}
		

_j2dir () { # likely one way transform for this, else make actual shell json parser: gross
	:
}

_j_escape () { # stdin esecaped Json -> escaped json
	 sed -e 's/\t/\\t/g' -e 's/"/\\"/g' $1 | awk -v ORS='\\n' '1' - | rev | cut -c3- | rev
}


_dir2ji () {  # (internal recursive ) directory to jsonify -> json string
	(cd $1
	sc='{'; ifmt='"%s": "%s",'; rfmt='"%s": '; ec='}'
	test -f .list && \
		{ sc='['; ifmt='"%s",'; rfmt=''; ec=']' ;}
	printf '%c\n' "$sc"
	for l in $(ls | sort -n)
	do
		ll="$l"; test -f .list && ll=''
		if test -f $l
		then
			printf "$ifmt" $ll "$(_j_escape $l)"
		else
			printf "$rfmt" $ll
			_dir2ji "$l"
		fi
	done
	printf '%c,' "$ec")
}
	
_dir2j () { # directory to jsonify -> json string
	_dir2ji "$@" | sed 's#\([]}"]\),\([]}]\)#\1\2#g;/$/ s#,$##' | sed 's#\([]}"]\),\([]}]\)#\1\2#g;/$/ s#,$##' | tr '\n' ' '
}

_shell () { # prompt character -> prompt word -> Monad shell
    _sPrompt=$(printf '[%-1.1s]< %-23.23s > -) ' "${1:-.}" \
		"${2:-$(echo $PWD | rev | cut -c-23 | rev)}")
	shift; shift
    case ${SHELL##*/} in
    bash)
		{ cat ${HOME}/.bashrc; printf 'PS1="%s"\n' "$_sPrompt" ;} >$tmp/bashrc
		bash --rcfile $tmp/bashrc "$@" 
		;;
    mksh)
		{ cat ${HOME}/.mkshrc; printf 'PS1="%s"\n' "$_sPrompt" ;} >$tmp/mkshrc
		ENV=$tmp/mkshrc mksh "$@"
		;;
    *) PS1="$_sPrompt" sh "$@" ;;
    esac </dev/tty >/dev/tty 2>/dev/tty
}

_conf () { # cmd -> name -> vars... -> Monad config
	mkdir -p ${SH_SHARE}/conf
	__CMD=$1; __NAME="$2"; shift; shift
    case "$__CMD" in
    list|'') ls -1t ${SH_SHARE}/conf; return 0 ;;
    load) test -f ${SH_SHARE}/conf/${__NAME} && . ${SH_SHARE}/conf/${__NAME} ;;
    dump) test -f ${SH_SHARE}/conf/${__NAME} && cat ${SH_SHARE}/conf/${__NAME} ;;
	exists) test -f ${SH_SHARE}/conf/${__NAME} ;;
    remove) rm ${SH_SHARE}/conf/${__NAME}; rm ${SH_BIN}/${__NAME} ;;
    *)
		test -e $__CMD || return 1
		__CMD=$(command -v $__CMD);
		truncate -s0 ${SH_SHARE}/conf/${__NAME}
		while test -n "$1"
		do
			printf '%s="%s"\n' "$1" "$(eval printf \$$1)" \
				>>${SH_SHARE}/conf/${__NAME}
			shift
		done
		(cd ${SH_BIN} && ln -sf ${__CMD} ${__NAME})
		;;
	esac
	unset __CMD __NAME
}

_r () { # Maybe len -> Maybe outType -> randomness
    dd if=/dev/urandom bs=4096 count=8 2>/dev/null \
	| { case ${2:-hex} in
	    alpha) tr -dc '[[:alpha:]]' ;;
	    alnum) tr -dc '[[:alnum:]]' ;;
	    hex) tr -dc 0-9a-f ;; 
	    int) tr -dc 0-9 ;; 
	    esac ;} | sed 's/^0*//' | cut -c-${1:-7}
}

_run_n () { # iterations -> command -> [command output]
    __N=$1; shift
    for _ in $(seq ${__N:-1}); do "$@"; done
	unset __N
}

_get_port () { # () -> Monad availablePort
    __PORT=$(( $(_r 3 int) * 48 + 1024 ))
    while nc -z localhost $__PORT
    do __PORT=$(( $(_r 3 int) * 48 + 1024 )); done
    echo $__PORT 
    unset __PORT
}

_row () {  # delim -> column names... -> rowOut
    __DELIM="$1"; shift
    __I=1;
    sed 's/^/./' | while read __REPLY
    do
		__REPLY="${__REPLY#.}"
	    test $__I -eq 1 && { printf "%s${__DELIM}" "$@" | sed 's/.$/\n/' ;}
	    if test $(( $__I % $# )) -eq 0
	    then printf "%s\n" "$__REPLY"
	    else printf "%s${__DELIM}" "$__REPLY"
	    fi 
		__I=$(( $__I + 1 ))
    done
    unset __DELIM __I __REPLY
}

_one_or_error () {  # stdin -> Monad (output, retcode)
    __TEMPFILE=$tmp/one_or_error_$(_r)
    tee $__TEMPFILE
    __CNT=$(<$__TEMPFILE wc -l); rm $__TEMPFILE
    case $__CNT in 0) return 1 ;; 1) return 0 ;; *) return $__CNT ;; esac
    unset __CNT __TEMPFILE
}

_gen_help () {  # Maybe filename -> helpOut
    __FNAME="${1:-$0}"
    printf 'FILE#%s#lines: %s\n' $__FNAME $(wc -l $__FNAME | cut -d' ' -f1)
	awk 'function print_doc (str) {
			n=split(str, a, "#"); if (n>2) { sub("^[[:space:]]*", "", a[n]); print(a[n]) }
			else print("")
		}
		/^[[:space:]]*##_/ { blockon=0 }
	    blockon { sub("[[:space:]]*# *", ""); printf("BLOCK#%s#%s\n", x, $0) }
	    /^[[:space:]]*##_[^[:space:]]/ { sub(".*##_", ""); blockon=1; x=$0 }

		/#[[:space:]]*[A-Z][A-Z]+/ { sub("^.*# *", ""); printf("%s#line: %s#", $1, NR)
								     sub("^[^[:space:]]* ?", ""); print $0 }

	    /^_[^[:space:]]+ \(\)/ { sub("^", "FUNCTION#"); printf("%s#", $1)
		    					 print_doc($0) }

        /^esac/ { cmdon=0 }
        cmdon && /^[^[:space:]]+)/ { sub("^", "COMMAND#"); sub("\\)$", "", $1); printf("%s#", $1)
   								     print_doc($0) }
        /^case/ { cmdon=1 }

		opton && /^done/ { opton=0 }
		opton && /^[[:space:]]+[[:graph:]]\)/ { sub("^[[:space:]]+", "OPTION#")
											    sub("\\)$", "", $1); printf("%s#", $1)
											    print_doc($0) }
	    /^while getopt/ { opton=1 }' $__FNAME 
    unset __FNAME
}

: ${__SHLIB_GIT_PUSH:=false}
: ${__SHLIB_PUSH_FORCE:=""}
: ${OGDIR:=$PWD}
OGDIR=$(_fullpath "$OGDIR")
: ${E:=${EDITOR:-vi}}
: ${SH_SHARE:=${HOME}/share}
: ${SH_BIN:=${HOME}/bin}
: ${SH_TESTDIR:=${OGDIR}/t}
mkdir -p "${OGDIR}/.log" 
#_with chan stdout 1 _log stdout
#_with chan stderr 2 _log stderr

# CONSTANTS
n3='[0-9][0-9][0-9]'
n4='[0-9][0-9][0-9][0-9]'
