: ${TERM:=dumb}
# TODO see if we can avoid this term stuff
export TERM

_comment () { # [comment char] -> Commented String
	sed "s?^?${1:-#} ?"
}

_bookend () {  # Monad Stdin -> width -> string -> String
	expand | sed 's/^/./' | while read __BOOKREAD
	do
		__BOOKREAD="${__BOOKREAD#.}"
    	printf -- "%s%-${1:-$(( $(tput cols) - 10 ))}.${1:-$(( $(tput cols) - 10 ))}s%s\n" \
            "${2:-|}" "${__BOOKREAD}" "${2:-|}"
	done 
	unset __BOORKREAD
}

_divider () {  # width -> string -> String
    printf -- "${2:-.}%0.0s" $(seq ${1:-$(( $(tput cols) - 10 ))}); echo
}

_trim () {
	sed 's/^[[:space:]]*//;s/*[[:space:]]$//'
}

_box () {  # Monad Stdin -> width -> corner string -> top string -> side string -> Box
    _divider "$1" "${3:--}" | _bookend "$1" "${2:-+}"
    _bookend "$1" "$4"
    _divider "$1" "${3:--}" | _bookend "$1" "${2:-+}"
}

_underline () {  # char -> String
	expand | sed 's/^/./' | while read __UNDERREAD
	do
		__UNDERREAD="${__UNDERREAD#.}"
		test -n "$__UNDERREAD" && echo "$__UNDERREAD"
		_divider $(expr length "$__UNDERREAD") "$1"
	done
	unset __UNDERLINE __UNDEREEAD
}

_ask () { # prompt -> headerFile -> width -> style -> Bool
	__CONT=true
	while $__CONT
	do
		test -f "$2" && cat "$2"
	    echo  ${1:-'[y/n]? '} | _box $3 $4 >/dev/tty
    	__C=$(_get_c | tr A-Z a-z)
        tput -x clear >/dev/tty
		case $__C in
		q) __CONT=false ;;
		n|N*) return 1 ;;
		y|Y*) return 0 ;;
		*) echo plz enter '"y"' or '"n"' | _box $3 $3 >/dev/tty;;
		esac
	done
}

_get_c () { # () -> get one character
    __OST=$(stty -F /dev/tty -g)
    stty -F /dev/tty raw -echo; dd if=/dev/tty bs=1 count=1 2>/dev/null
    stty -F /dev/tty $__OST
    unset __OST
}

_toggle_mark () { # file -> pos -> mark -> Maybe () -> Monad mark set
    if sed -n "${2}p" "$1" | grep -q "^${3}"
    then
        test ${4:+true} && return 0
        sed -i "$2 s/^${3}//" "$1"
    else
        ${4:+true} || return 0
        sed -i "$2 s/^/${3}/" "$1"
    fi
}

_focus () { # index -> height -> width -> style -> Monad Interact
    cat - >$tmp/focus
    __FI=${1:-1}; __FH=$2 __FW=$3
    test $__FI -le 0 && __FI=1
    __FMAX=$(<$tmp/focus wc -l); test $__FI -ge $__FMAX && __FI=$__FMAX
    __FMID=$(( $__FH / 2 ))
    if test $__FI -le $__FMID  
    then
        __FTOFF=1; __FBOFF=$__FH
    elif test $__FI -gt $(( $__FMAX - $__FMID ))
    then
        __FTOFF=$(( $__FMAX - $__FH ))
        test $__FTOFF -le 0 && __FTOFF=1
        test $(( $__FH % 2 )) -ne 0 && __FTOFF=$(( $__FTOFF + 1 ))
        __FBOFF=$__FMAX
    else
        __FTOFF=$(( $__FI - $__FMID ))
        test $(( $__FH % 2 )) -ne 0 && __FBOFF=$(( $__FI + $__FMID )) \
            || __FBOFF=$(( $__FI + $__FMID - 1 ))
    fi
    { test $__FI -ne 1 && <$tmp/focus sed -n \
            "${__FTOFF},$(( $__FI - 1 ))p" | _bookend "$__FW" .
      <$tmp/focus sed -n ${__FI}p | _box "$__FW" "$4" "$5" "$6"
      test $__FI -ne $__FMAX && test $__FBOFF -ne 1 && <$tmp/focus sed -n \
          "$(( $__FI + 1 )),${__FBOFF}p" \
            | _bookend "$__FW" . ;} 
    unset __FW __FI __FH __FMAX __FMID __FTOFF __FBOFF
}

_choose () {  # prompt -> index -> display-func -> width -> corner -> top -> side -> Monad Selector
    tee $tmp/chosen_raw >$tmp/chosen
    test $(<$tmp/chosen_raw wc -l) -ge 1 || { echo no input; return 1 ;}
    __CI=${2:-1}; __CR="--> "
    while true
    do
        tput -x clear >/dev/tty
        { printf "%s\n" "$1" | _box "$4" "$5" "$6" "$7"
          <$tmp/chosen _focus $__CI $(( $(tput lines) / 3)) "$4" "$5" "$6" "$7" 
          printf -- '[%0.3d]%s\n' "$__CI" \
            "[jkJK] scroll. [a] all. [q] quit [m] mark" | _box "$4" "$5" "$6" "$7" ;} >/dev/tty
        ${3:-echo} $(sed -n ${__CI}p $tmp/chosen_raw) >/dev/tty
        case $(_get_c) in
        a) tput -x clear >/dev/tty; cat $tmp/chosen_raw; break ;;
        k) __CI=$(( $__CI - 1 )) ;;
        j) __CI=$(( $__CI + 1 )) ;;
        K) __CI=$(( $__CI - 10 )) ;;
        J) __CI=$(( $__CI + 10 )) ;;
        m) _toggle_mark $tmp/chosen $__CI "$__CR" ;;
        q|'') break ;;
        *) 
            _toggle_mark $tmp/chosen $__CI "$__CR" on
	        tput -x clear >/dev/tty
            sed -n "/^$__CR/ s/^$__CR//p" $tmp/chosen
            break
            ;;
        esac
        test $__CI -lt 1 && __CI=1
        test $__CI -gt $(<$tmp/chosen wc -l) && __CI=$(<$tmp/chosen wc -l)
    done 
    unset __CI __CR
}

_spin () { # index -> number -> spinner -> String
    __SL=$(expr length "${3:-0123456789}")
    __SN=${2:-1}
    while test $__SN -gt 0
    do
        __SN=$(( $__SN - 1 ))
        sIdx=$(( ($(echo "$1 / $__SL ^ $__SN" | bc) % $__SL) + 1 ))
        printf '%c' $(expr substr "${3:-0123456789}" \
            $(( ($(echo "$1 / $__SL ^ $__SN" | bc) % $__SL) + 1 )) 1)
    done
    unset __SL __SN
}

_clear () {
    { printf '\r'; printf ' %0.0s' $(seq $(tput cols)); printf '\r' ;} >/dev/tty
}

_scan () { # message  -> (index, scan output to tty)
	__SW=$(tput cols)
	__SW=$(( $__SW - 33 ))
	__HALF=$(( $__SW / 2 ))
	: ${i:=1}; : ${s:=+}
	i=$(printf '%f %s %f * %f\n' $i "$s" 0.3 $i | bc -l)
	i=$(printf "%f" $i)
	test ${i%%.*} -lt 1 && { s="+"; i=1 ;}
	test ${i%%.*} -ge "$__SW" && { case $s in +) s="-" ;; *) s="+" ;; esac; continue ;}
	_clear
	{ x=$(printf ".%0.0s" $(seq $(( ${i%%.*} / 2 )) )); printf "%${__HALF}.${__HALF}s[%-33.33s]%s\r" $x "$1" $x ;}
}

