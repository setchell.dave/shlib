PREFIX := ${HOME}
SH_BIN := ${PREFIX}/bin
SH_SHARE := ${PREFIX}/share
bin := shrun.sh trun.sh
lib := .*.sh

install:
	mkdir -p ${SH_SHARE} ${SH_BIN}
	cp -ra ${lib} ${SH_SHARE}
	cp ${bin} ${SH_BIN}
	sed -i -e 's#@sharedir@#${SH_SHARE}#' \
			-e 's#@bindir@#${SH_BIN}#' ${SH_BIN}/shrun.sh ${SH_BIN}/trun.sh
	@echo installed to ${PREFIX}

clean:
	@echo do some cleaning
    
.PHONY: install clean
