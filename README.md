# shlib

various nice shell functions for sharing

func
----
- cleanup
- output
- with context interface
- with contexts
- json
- sub shell
- configuration
- misc
-- random
-- run n
-- lines to table ( row )
-- get port
-- error
- help
- testing

msg 
---


FILE      lines: 369            /home/dave/repo/shlib/.func.sh
FUNCTION  _pre_clean            redefine for your app
FUNCTION  _cleanup              called on exit
TODO      line: 20              dangerous run forever
FUNCTION  _post_clean           redefine for your app
FUNCTION  _form                 stdin:format -> args... -> formatted output
FUNCTION  _error                args -> echos args to error channel and exits
FUNCTION  _log                  level -> msg -> log written
FUNCTION  _log_pipe             [level] -> tee file -> log
FUNCTION  _with                 ctx -> args -> Monad context 
FUNCTION  _with_top             () -> top of context args
FUNCTION  _with_out             args... -> pop ctx to args...
TODO      line: 72              seems a bit heafty to handle a pop chain
FUNCTION  _in_stash             stash name -> in stash context
FUNCTION  _out_stash            stash name -> popped out
FUNCTION  _branch_exists        [branch name] -> boolean
FUNCTION  _in_branch            branch name -> checked out ctx
FUNCTION  _out_branch           Env vars -> branchname -> out of branch context
FUNCTION  _in_loopback          file -> mountName -> Monad mounted
FUNCTION  _out_loopback         () -> Monad unmounted
FUNCTION  _in_fifo              name -> loglevel -> target fd -> tmp fd -> fifo context
FUNCTION  _out_fifo             name -> loglevel -> target fd -> tmp fd -> pid -> end fifo context
FUNCTION  _j2dir                likely one way transform for this, else make actual shell json parser: gross
FUNCTION  _j_escape             stdin esecaped Json -> escaped json
FUNCTION  _dir2ji               (internal recursive ) directory to jsonify -> json string
FUNCTION  _dir2j                directory to jsonify -> json string
FUNCTION  _shell                prompt character -> prompt word -> Monad shell
FUNCTION  _conf                 cmd -> shdir -> bindir -> name -> vars... -> Monad config
FUNCTION  _r                    Maybe len -> Maybe outType -> randomness
FUNCTION  _run_n                iterations -> command -> [command output]
FUNCTION  _get_port             () -> Monad availablePort
FUNCTION  _col                  delim -> column output
FUNCTION  _row                  delim -> column names... -> rowOut
FUNCTION  _one_or_error         stdin -> Monad (output, retcode)
FUNCTION  _gen_help             Maybe filename -> helpOut
FUNCTION  _tests                
FUNCTION  _test_wrapper         
FILE      lines: 140            /home/dave/repo/shlib/.msg.sh
FUNCTION  _br                   variety -> style -> Char
FUNCTION  _bookend              Monad Stdin -> width -> variety -> style -> String
FUNCTION  _divider              width -> style -> String
FUNCTION  _box                  Monad Stdin -> width -> style -> String
FUNCTION  _underline            Bool -> width -> style -> String
FUNCTION  _ask                  prompt -> width -> style -> Bool
FUNCTION  _get_c                wow dude wtf
FUNCTION  _toggle_mark          file -> pos -> mark -> Maybe () -> Monad mark set
FUNCTION  _focus                index -> height -> width -> style -> Monad Interact
FUNCTION  _choose               prompt -> index -> display-func -> style -> Monad Selector
FUNCTION  _spin                 index -> number -> spinner -> String
FUNCTION  _clear                
FILE      lines: 144            /home/dave/repo/shlib/t.sh
FUNCTION  __test_with_stash     
FUNCTION  __test_with_branch    
FUNCTION  __test_with_loopback  
FUNCTION  __test_conf           
FUNCTION  __test_r              
FUNCTION  __test_run_n          
FUNCTION  __test_get_port       
FUNCTION  __test_row            
FUNCTION  __test_one_or_error   () -> Bool
FUNCTION  __test_br             
FUNCTION  __test_bookend        
FUNCTION  __test_divider        
FUNCTION  __test_box            
FUNCTION  __test_underline      
FUNCTION  __test_ask            
FUNCTION  __test_fail           
FUNCTION  _show_gred            
OPTION    h                     foo
COMMAND   test                  some workds
COMMAND   conf                  some other news
COMMAND   conf-list             
COMMAND   conf-dump             
COMMAND   grep                  
COMMAND   *                     
