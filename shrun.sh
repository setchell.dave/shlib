#!/bin/sh
: ${SH_SHARE:=@sharedir@}
: ${SH_BIN:=@bindir@}
test "$SH_SHARE" != ${SH_SHARE#@} && SH_SHARE=$PWD
test "$SH_BIN" != ${SH_BIN#@} && SH_BIN=$PWD

for f in $(ls ${SH_SHARE}/.*.sh)
do . $f; done

_show_gred () {
	n=${2#+}
	test 3 -ge ${n:=0} && n=3
	sed -n "$(( $n - 3 )),$(( $n - 1 )) p" "$3"
	sed -n "${n}p" "$3" | _comment ">>> "
	sed -n "$(( $n + 1 )),$(( $n + 3 )) p" "$3"
}

while getopts h arg
do
    case "$arg" in
    h) # foo
        for f in $(ls ${SH_SHARE}/.*.sh)
        do _gen_help $f; done
		_gen_help
        exit 0
        ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1

: ${cmd:=${1:-r}}; test -n "$1" && shift
case "$cmd" in
grep) # file pattern -> match -> Monad choose
	find . -type f -name "$1" | xargs -r grep -rn "$2" \
		| awk -F: '{ print "vis +"$2, $1}' | _choose "choose edit" 1 _show_gred ""
	;;
chan)
	_with chan lol "" echo 
	printf '%s\n' foo boo roo | _cmd_chan lol in
	;;
?stash|?branch|?chan)
	wcmd=${cmd#?}
	_with $wcmd "$@"
	case "$cmd" in
	?chan) _cmd_chan $1 in ;;
	p*) sh -s ;;
	*) _shell . $1 ;;
	esac
	;;
*)
	_${cmd} "$@"
	;;
esac
sync

