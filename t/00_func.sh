__test_form () {
	$shrun form ichi ni san go roku shichi hachi kyuu juu <<EOF
one %s two %s three %s
four %s five %s six %s
seven %s eight %s nine %s\n
EOF
}

__test_with_stash () {
	git clone git@gitlab.com:setchell.dave/shlib >/dev/null 2>&1 || _error failed to clone
	cd shlib
	# clean dir ( aka nothing todo )
	_with stash empty || echo avoid with joy
	echo checking dirting working directory | _underline
	echo im never gonna dance again >>README.md
	$shrun pstash working <<EOF
git stash list | grep working
EOF
	echo checking untracked files | _underline
	echo guilty feet have got no rhythm >>FAKEFILE
	$shrun pstash untracked <<EOF
git rev-list -g stash | git rev-list --stdin --max-parents=0 | xargs git log --pretty=format: --name-only
EOF
	echo checking ignored files | _underline
	echo i know you are not a fool >testfake
	$shrun pstash fakes <<EOF
git rev-list -g stash | git rev-list --stdin --max-parents=0 \
		| xargs git log --pretty=format: --name-only
EOF
}

__test_with_branch () {
	git clone git@gitlab.com:setchell.dave/shlib >/dev/null 2>&1 || _error failed to clone
	cd shlib
	echo creating test branch | _underline
	$shrun pbranch testing-with-context <<EOF
git branch -a | grep '[[:space:]]*\*'
EOF
	echo never gonna give you up >>README.md
	$shrun pbranch local-changes <<EOF
git branch -a | grep '^[[:space:]]*\*'
EOF
	sed -n '$p' <README.md
	{ git branch -D testing-with-context
	git branch -D local-changes ;} >/dev/null 2>&1
}

__test_with_loopback () {
#	# TODO set-up file
#	test -f $TD/alpine.iso || _wait curl -L http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/x86_64/alpine-standard-3.12.0-x86_64.iso >$TD/alpine.iso
#	_with loopback alpine $TD/alpine.iso iso
#	_cmd_loop alpine tree
#	return 0
	:
}

__test_with_clone () {
	:
}

__test_chan () {
	echo making channel grue | _underline
	$shrun pchan grue "" printf 'from chan: %s\n' <<EOF
lg and samsun relocation production to 
another country. A country called china. 
Have you heard of it.
They were a joke.
EOF
}

__test_wait () {
	$shrun wait sleep 10
}

__test_dir2j () {
	mkdir -p dir2j
	cd dir2j
	for x in one two three
	do
		for y in $(seq 3)
		do
			mkdir -p $x/$y
			touch $x/.list
			for z in eleven and heaven without our brethren
			do
				echo so much awesome	text >$x/$y/$z
			done
		done
	done
	tree .
	$shrun dir2j . | json_pp
}
				
__test_conf () {
	a=one b=two c=three $shrun conf $shrun test a b c
	$shrun conf dump test
}

__test_r () {
	truncate -s 0 test.randos
	for _ in $(seq 10)
	do
		$shrun r $_ alpha | tee -a test.randos | tr -d '[[:alpha:]]*'
		$shrun r $_ alnum | tee -a test.randos | tr -d '[[:alnum:]]*'
		$shrun r $_ int | tee -a test.randos | tr -d '[0-9]*'
		$shrun r $_ hex | tee -a test.randos | tr -d '[[:xdigit:]]*'
	done | sed '/^[[:space:]]*$/d'
	<test.randos wc
}

__test_run_n () {
	for __I__ in $(seq 3 5)
	do
		$shrun run_n $__I__ echo running this $__I__ times
	done
}

__test_get_port () {
	for p in $($shrun run_n 10 _get_port)
	do nc -z localhost $p && { echo $p is taken; return 1 ;}
	done
	return 0
}

__test_row () {
	seq 21 | $shrun row . one two three
}

__test_one_or_error () { # () -> Bool
	{ printf '%s\n' foo boo | $shrun one_or_error ||: ;} \
		&& { printf '%s\n' moo | $shrun one_or_error ;}
}

