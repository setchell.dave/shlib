_init_test () {
	(cd $TD; git clone git@gitlab.com:setchell.dave/shlib \
		|| _error failed to clone)
}

_clean_test () {
	:
}

__test_comment () {
	echo one line | _comment
	_comment '//' <<EOF
some lame text
that no one should
read that explains stuff
EOF
	_comment '--' <<EOF
you know. Just comments and
all their wiley ways. Let's 
leave them
out of our code
EOF
	_comment <<EOF
or at least
let them be any character
they like
EOF
}

__test_bookend () {
	for v in ^ '*' '.'
	do
		tree -L 2 | _comment " " | _bookend 23 "$v"
	done
}

__test_divider () {
	for i in '-' 'x' '.' '*' '!'
	do
		_divider 23 "$i"
	done
}

__test_box () {
	for i in  '-' 'x' '.' '*' '!'
	do
		tree -L 2 | expand | _box 80 "$i" "$i"
	done
}

__test_underline () {
	<<EOF _form one two three | _underline _
[%s] is the first
and then [%s] too
again yet [%s]
EOF
	<<'EOF' _form four five six seven | _underline ^
some text
<<%s>> [[ %s ]]
for formatting
EOF
}

