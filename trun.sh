#!/bin/sh
: ${SH_SHARE:=@sharedir@}
: ${SH_BIN:=@bindir@}
test "$SH_SHARE" != ${SH_SHARE#@} && SH_SHARE=$PWD
test "$SH_BIN" != ${SH_BIN#@} && SH_BIN=$PWD

for f in $(ls ${SH_SHARE}/.*.sh)
do . $f; done

_init_test () { :; }
_clean_test () { :; }

__clean_test () {
	$KEEPSTATE && return 0
	_clean_test
	rm -rf $TESTDIR/.workspace/*
}

_setup_workspace () {
	_TD="$TESTDIR/.workspace"
	test -d "$_TD" || mkdir -p "$_TD"
	TD=""; TD=$(ls -t "$_TD" | head -1)
	$OLDSTATE && test -n "$TD" && TD="${_TD}/${TD}" && _ask "use old dir: $_TD ?" \
		|| { TD=${_TD}/$(_r); mkdir -p $TD; . $TESTDIR/.init.sh ;}
}


_tmatch () { # match -> testfile list
	ls $TESTDIR/*.sh | sort -n | grep "${1:-.*}"
}		

_tests () {
	for tf in $(_tmatch "$1")
	do
		_gen_help "${tf}" | grep '^FUNCTION' | cut -d'#' -f2 \
		| sed -n '/^__test/ s/^__test_//p'
	done
}

_show_file () {
	:
}

_regression () { # tgroup -> tname
	mkdir -p $TESTDIR/regression/$1/$2
	for outf in $(ls $O/$1/$2 | grep -v error) error # error last
	do
		${VERBOSE} &&
			{ echo " [${1}::${2}] file: $outf" | _underline | cat - $O/$tgroup/${2}/$outf | _comment ;}
		case $outf in *message) continue ;; esac # maybe other names to skip
		$SET_REGRESSION && cp $O/$1/$2/$outf $TESTDIR/regression/$1/$2/$outf
		test -f $TESTDIR/regression/$1/$2/$outf -a -f $O/$1/$2/$outf || continue
		# NOTE could have fallthrough instead of eject on error
		if ! cmp -s $TESTDIR/regression/$1/$2/$outf $O/$1/$2/$outf
		then
			$VERBOSE && { echo " [${1}::${2}] regression failed for: $outf" | _underline
				diff $TESTDIR/regression/$1/$2/$outf $O/$1/$2/$outf ;} | _comment
			return 1
		fi
	done
	return 0
}

_test_wrapper () { # tnum -> tfile -> tname
	tfile="${TESTDIR}/$2"; test -f "$tfile" || return 1
	tgroup=$(basename $tfile); tgroup=${tgroup%.*};
	. $tfile; retcode=""; input=/dev/tty
	test -f "$TESTDIR/input/${tgroup}/${3}" && input="$TESTDIR/input/${tgroup}/${3}"
	mkdir -p $O/$tgroup/$3
	(cd $TD; <$input __test_${3} $O/$tgroup/$3 >$O/$tgroup/${3}/stdout 2>$O/$tgroup/${3}/error)
	retcode=$?
	_clear
	test ${retcode:-127} -eq 0 && { _regression $tgroup $3; retcode=$? ;}
	case $retcode in
	0) printf 'ok %d [%s::%s] %s\n' $1 $tgroup $3 success ;;
	*)
		printf 'not ok %d [%s::%s] %s\n' $1 $tgroup $3 fail 
		<$O/$tgroup/${3}/error _comment
		${EXIT_ON_ERROR:-false} && { __clean_test; _error "#test $3 failed exit requested" ;}
		;;
	esac
	if $INTERACTIVE
	then
		(cd $TD; _shell)
	fi
}

while getopts hvrd:xoik arg
do
    case "$arg" in
	v) VERBOSE=true ;;
	d) TESTDIR="$OPTARG" ;;
	r) SET_REGRESSION=true ;;
	x) EXIT_ON_ERROR=true ;;
	i) INTERACTIVE=true ;;
	k) KEEPSTATE=true ;;
	o) OLDSTATE=true ;;
    h) _gen_help ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${OLDSTATE:=false}
: ${VERBOSE:=false}
: ${TESTDIR:=./t}
: ${SET_REGRESSION:=false}
: ${EXIT_ON_ERROR:=false}
: ${INTERACTIVE:=false}
: ${KEEPSTATE:=false}

mkdir -p $TESTDIR/regression $TESTDIR/output
O=$(_fullpath $TESTDIR/output)

: ${cmd:=${1:-tests}}; test -n "$1" && shift

test -z "$1" && set -- .
for m in "$@"
do
	tf=".*"; tm="$m"
	case "${m}" in *:*) tf="${m%:*}"; tm="${m#*:}" ;; esac 
	for f in $(_tmatch "$tf")
	do
		_tests $f | grep "$tm" | sed "s#^#$(basename $f):#" >>$tmp/testplan
	done
done
	
case "$cmd" in
tests)
	cat $tmp/testplan
	;;
run) 
	_setup_workspace
	tnum=$(<$tmp/testplan wc -l)
	test "$tnum" -le 0 && return 1
	printf "%d..%d\n" 1 "$tnum"; i=0
	for test in $(nl -s: $tmp/testplan)
	do
		args=$(echo "$test" | tr ':' ' ')
		(_test_wrapper $args)
	done
	__clean_test
	;;
*) _$cmd "$@" ;;
esac

